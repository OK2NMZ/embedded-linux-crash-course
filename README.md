# Embedded Linux Crash Course #
![logos](./www/img/logos.png)

## FAQ
### What is this?
This course is intended for those who are interested in Embedded Linux. It is based on tutorials with affordable [UDOO Neo board](http://www.udoo.org/udoo-neo/). The tutorials are sorted according to their complexity. At the end of this course, you should be able to create your own Embedded Linux based project. You should be also able to identify applications, which can benefit from the usage of Embedded Linux over usage of simple bare-metal microcontroller based platform. You will also learn, how to combine these two thanks to the AMP (asymmetric multiprocessing) leveraging the on-chip Cortex-M4, which is part of i.MX6SX SoC used in the [UDOO Neo board](http://www.udoo.org/udoo-neo/). 

This course is open for the community to contribute both in the tutorials and to the source codes associated with the tutorials. In case of any further questions write directly to the maintainer of the project - [Marek NOVAK](marek.novak@nxp.com).

### How can I use it?
Simply go through the tutorials and follow the step-by-step guides to make things working. Feel free to contribute and provide links to your projects created thanks to this tutorial to give use some credit in reward :-).

## Hands-On's: 
- Remote Processor Messaging (RPMsg) on UDOO Neo: [step-by-step Guide](./HandsOn_remote_processor_messaging/README.md) (by Marek NOVAK)

## Interesting Links

- [Running RPMsg Demo Applications for Multicore Communication with IMX6SX and IMX7D](https://community.nxp.com/docs/DOC-333803)
- [RPMsg implementation for small MCUs, RPMsg-Lite](https://github.com/NXPmicro/rpmsg-lite)
- [Embedded Remote Procedure Call library](https://github.com/EmbeddedRPC)
- [Cortex-M4 in i.MX7D](http://developer.toradex.com/knowledge-base/freertos-on-the-cortex-m4-of-a-colibri-imx7)