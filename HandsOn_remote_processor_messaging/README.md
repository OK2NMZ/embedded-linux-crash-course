# Using RPMsg and eRPC on UDOO Neo #
- First, download the UDOObuntu image: https://sourceforge.net/projects/udooboard/files/UDOO_Neo/UDOObuntu/udoobuntu-udoo-neo-desktop_2.1.2.zip/download
- Unzip the file
- Then use dd or simillar utility to copy udoobuntu-udoo-neo-desktop_2.1.2.img to your empty SDCARD (of size >= 4GB):  
`dd if=udoobuntu-udoo-neo-desktop_2.1.2.img of=/dev/mmcblk0 bs=10M && sync`
- Now, to add RPMsg support we need to change U-Boot and Linux Kernel:  

## U-Boot Modification ##
- Clone u-boot for UDOO Neo:  
`git clone https://github.com/UDOOboard/uboot-imx`
- Then checkout at SHA b80fafce2eb96b0aad03:  
`git checkout b80fafce2eb96b0aad03`
- Then copy add_m4_in_ocram_with_rpmsg.patch in uboot-imx directory
- From uboot-imx folder, apply the patch:  
`git apply add_m4_in_ocram_with_rpmsg.patch`
- Then define your architecture:  
`export ARCH=arm`
- Then provide path to your compiler toolchain:  
`export CROSS_COMPILE=~/bin/gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-`
- Then create .config file for the u-boot build run:  
`make udoo_neo_defconfig`
- Then start the build:  
`make`
- Have a `coffee...`
- Assuming /dev/mmcblk0 is your device containing the UDOObuntu image, run this:  
`sudo dd if=SPL of=/dev/mmcblk0 bs=1K seek=1`  
`sudo dd if=u-boot.img of=/dev/mmcblk0 bs=1K seek=69`  
`sync`
- Now, new u-boot supporting bootaux command is ready...

## Linux Kernel Modification ##
- Clone Linux Kernel for UDOO Neo:  
`git clone https://github.com/UDOOboard/linux_kernel`
- Then checkout at SHA eaaf16e8bc137b0bd2a737:  
`git checkout eaaf16e8bc137b0bd2a737`
- Then copy add_rpmsg_imx_rpmsg_tty_udoo_neo.patch in linux_kernel directory
- Apply the patch:  
`git apply add_rpmsg_imx_rpmsg_tty_udoo_neo.patch`
- Then define your architecture:  
`export ARCH=arm`
- Then provide path to your compiler toolchain:  
`export CROSS_COMPILE=~/bin/gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-`
- Then create .config file for UDOO Neo:  
`make udoo_neo_defconfig`
- Then start the build:  
`make`
- Have a `bigger coffee...`
- Then bootup your UDOO Neo board, let it boot and then the board should appear as a mass storage device - copy zImage from `arch/arm/boot/zImage` to `/boot` partition and replace the existing file. Then copy `imx6sx-udoo-neo-full-hdmi-m4-rpmsg.dtb` from `arch/arm/boot/dts/imx6sx-udoo-neo-full-hdmi-m4-rpmsg.dtb` to `/boot` partition to `/dts/`.
- You will also need modules build along with your modified Linux Kernel, first create lib folder by running:  
`INSTALL_MOD_PATH=OUTPUT make modules_install`
- Turn off UDOO Neo board, put SDcard in your PC reader.
- Copy lib folder to your Linux Kernel root file system located on the SDcard and merge the lib folder located in OUTPUT folder with the lib folder already present on the SDcard.

## Build M4 Image ##
- Clone `erpc-imx-demos` repository:  
`git clone https://github.com/EmbeddedRPC/erpc-imx-demos.git`
- Open `erpc-imx-demos` folder and update submodules:  
`git submodule update --init --recursive`
- Install this toolchain, by extracting it to some YOUR_INSTALL_DIRECTORY: https://launchpad.net/gcc-arm-embedded/5.0/5-2015-q4-major
- Setup ARMGCC_DIR path: `export ARMGCC_DIR=YOUR_INSTALL_DIRECTORY`
- Enter directory MCU/example_rpmsg/build/armgcc/imx6sx_sdb_m4 and run `build_all.sh` script.
- In `/debug` folder, you will locate `rpmsg_mcu_mpu_hello_world.bin` file - copy it to `/boot` partition of your SDcard and rename it to `m4.bin`.
- If `/boot` partition does not mount, you can access it after connecting UDOO Neo and booting it up. 

## Test RPMsg works ##
- Use USB/UART(TTL) converter and connect to debug port of your UDOO Neo board
- Use putty or screen to open the serial link:  
`sudo screen /dev/ttyUSB0 115200`
- Reset your UDOO Neo board, hit enter before the boot starts and then enter:  
`run myboot`
- This modified boot script will load `m4.bin` firmware for the Cortex-M4 and then use a custom device tree  `imx6sx-udoo-neo-full-hdmi-m4-rpmsg.dtb` and then it will boot linux.
- On a successfull boot-up and login (`udooer:udooer`), type: `dmesg | grep rpmsg` and you should see a nameservice communication between M4 and A core.
- type `sudo modprobe imx_rpmsg_tty` to load a kernel module exporting RPMsg to user space using TTY device - a device will appear: `/dev/ttyRPMSG`
- use python, shell or C to read and write to this character device to communicate with the remote core, modify the project for M4 core... make a nice example using M4
- For a simple testing, you can install the screen utility by running from your UDOO Neo (you need your UDOO Neo to be online for this, of course...):  
`sudo apt-get install screen`
- And then:  
`sudo screen /dev/ttyRPMSG`
- Now, if you type anything, the Cortex-M4 will answer with a predefined message - you can play modifying the firmware for Cortex-M4 and make it use somehow the message content sent to it from Linux.
- Buy a champagne to celebrate a succesfull run of this long step-by-step guide :-)
